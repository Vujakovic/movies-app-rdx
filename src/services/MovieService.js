import HttpService from "./HttpService";

class MovieService extends HttpService {
getAll = async () =>{
    const { data } = await this.client.get("movies");
    return data;
  }

  async get(id) {
    const { data } = await this.client.get(`movies/${id}`);
    return data;
  }
  

  addMovie = async (movie) =>{
    
    const  {data}  = await this.client.post("movies",movie);
  
    return data;
  }


}

const movieService = new MovieService();
export default movieService;