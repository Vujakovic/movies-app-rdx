import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import './App.css';
import Nav from './components/Nav';
import AppMovies from './pages/AppMovies';


function App() {

  
  return (
    <div className="App">
        <Router>
            <Nav/>

            <div className="container">
            <Switch>
                
                <Route path='/movies' exact>
                    <AppMovies/>
                </Route>

                <Route path='/' redirect exact>
                    <Redirect to="/movies" />
                </Route>
              
            </Switch>
            </div>
          </Router>

    </div>
  );
}

export default App;
