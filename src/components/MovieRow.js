import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { isMovieSelected } from '../store/movies/selectors'
import { setToggleSelected } from '../store/movies/slice'



function MovieRow({movie}) {
  const dispatch = useDispatch()
  const selected = useSelector(state=>isMovieSelected(state,movie.id))

  return (

    <div className="card m-2">
        
        <div className="card-body d-flex flex-column justify-content-center align-items-center">

        <input style={{width:"20px", height:"20px"}} className='d-block mb-2' checked={selected} type="checkbox" onChange={()=>dispatch(setToggleSelected(movie.id))}/>

        <img style={{width:"250px"}} className="card-img-top" src={movie.imageUrl} alt={movie.imageUrl}/>
            <h5 className="card-title">{movie.title}</h5>
            <p><small>Director:</small> {movie.director}</p>
            <p><small>Duration:</small> {movie.duration}</p>
            <p><small>Release date: { movie.releaseDate}</small> </p>
            <p><small>Genre:</small> {movie.genre}</p>
        </div>
    </div>
 
  )
}

export default MovieRow
