import React from 'react'
import { Link } from 'react-router-dom'


function Nav() {
  return (
    <div className='bg-dark mb-2'>
        <ul className="nav">
            <li className="nav-item">
                <Link to="/movies" className="nav-link text-white">Movies</Link>
            </li>

        </ul>
    </div>
  )
}

export default Nav