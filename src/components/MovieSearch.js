import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { selectSearch } from '../store/movies/selectors'
import { setSearchedText } from '../store/movies/slice'



function MovieSearch() {
    const searched = useSelector(selectSearch)
    const dispatch = useDispatch()
    

  return (
    <div>
       <div className="input-group rounded mb-5">
            <input value={searched} onChange={({target})=>dispatch(setSearchedText(target.value))} style={{height: "50px"}} type="search" className="form-control rounded" placeholder="Search" aria-label="Search" aria-describedby="search-addon" />
        </div>
  </div>
  )
}

export default MovieSearch