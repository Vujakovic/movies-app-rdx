import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { pageNumber } from '../store/movies/selectors'
import { pageNumberDecrement, pageNumberIncrement } from '../store/movies/slice'



function Pagination() {
const dispatch =useDispatch()
const pageNum = useSelector(pageNumber)
  return (
    <div>

        <strong>Page number {pageNum}</strong>
        <nav aria-label="Page navigation example">
            <ul className="pagination">
                <li className="page-item">
                <button className="page-link" onClick={()=>dispatch(pageNumberDecrement())}>
                    <span aria-hidden="true">&laquo;</span>
                    <span className="sr-only">Previous</span>
                </button>
                </li>
            
                <li className="page-item">
                <button className="page-link" href="#" aria-label="Next" onClick={()=>dispatch(pageNumberIncrement())}>
                    <span aria-hidden="true">&raquo;</span>
                    <span className="sr-only">Next</span>
                </button>
                </li>
            </ul>
        </nav>
    </div>
  )
}

export default Pagination