import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { addMovie } from '../store/movies/slice'

function AddMovie() {
const [movie, setMovie] = useState({
    title: "", 
    director:"", 
    imageUrl: "",
    duration: 0,
    releaseDate:"", 
    genre:""

})

const dispatch = useDispatch()

const handleSubmit = (e) => {
  e.preventDefault()
  dispatch(addMovie(movie))

 
}

  return (
    <div>
        <form onSubmit={(e)=>handleSubmit(e)}>
            <input className='m-2' placeholder='Title...' type="text" onChange={({target})=>setMovie({...movie, title:target.value})} />
            <input className='m-2' placeholder='Director...' type="text" onChange={({target})=>setMovie({...movie, director:target.value})} />
            <input className='m-2' placeholder='image' type="text" onChange={({target})=>setMovie({...movie, imageUrl:target.value})} />
            <input className='m-2' placeholder='Duration...' type="number" onChange={({target})=>setMovie({...movie, duration:target.value})} />
            <input className='m-2' placeholder='Release date...' type="date" onChange={({target})=>setMovie({...movie, releaseDate:target.value})} />
            <input className='m-2' placeholder='Genre' type="text" onChange={({target})=>setMovie({...movie, genre:target.value})} />

            <button className='btn btn-danger' type="submit">Add movie</button>
        </form>
    </div>
  )
}

export default AddMovie