import { createSlice } from "@reduxjs/toolkit";

const middlewareActions = {
  getMovies: () => {},
  addMovie: () => {}
};


export const moviesSlice = createSlice({
  name: "movies",
  initialState: {
    movies: [],
    searchedText:"",
    selectedMovies: [],
    filter:"",
    pageNum:1,
    errors:{},
  },
  reducers: {
    setAllMovies: (state, action) => {
        state.movies = action.payload;
      },
    addNewMovie: (state,action) => {
      state.movies = [...state.movies, action.payload]
    },
    setSearchedText: (state,action)=>{
        state.searchedText = action.payload
    },
    setToggleSelected: (state, action)=>{
        if(state.selectedMovies.some(id=>id===action.payload)){
          state.selectedMovies = state.selectedMovies.filter(item=>item!==action.payload)
        }else{
          state.selectedMovies.push(action.payload)
        }
    },
    deselectAll: (state) => {
      state.selectedMovies = [];
    },
    selectAll: (state) => {
      state.selectedMovies = state.movies.map((movie) => movie.id);
    },
    setFilter: (state, action) => {
      state.filter= action.payload
    },
    pageNumberIncrement: (state)=> {
      if(state.pageNum*5 < state.movies.length){
        state.pageNum += 1
      }
    },
    pageNumberDecrement: (state)=> {
      if(state.pageNum>1){
        state.pageNum-=1
      }
    },
    
    ...middlewareActions,
  },
});

export const { setAllMovies, setSearchedText, setToggleSelected, 
              deselectAll, selectAll, setFilter, pageNumberIncrement, 
              pageNumberDecrement, getMovies, addMovie, addNewMovie} = moviesSlice.actions;

export default moviesSlice.reducer; //
