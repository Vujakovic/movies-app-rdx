import {call, put, takeLatest} from "redux-saga/effects"
import movieService from "../../services/MovieService";
import { addMovie, addNewMovie, getMovies ,setAllMovies} from "./slice";



function* getMoviesHandler() {
    const data = yield call(movieService.getAll)
    
    yield put(setAllMovies(data))
}

function* setMovieHandler(action){
    try {
        const data = yield call(movieService.addMovie,action.payload)
        yield put(addNewMovie(data))
    } catch (error) {
        console.log(error);
    }
  
}


export function* watchForSagas(){
    yield takeLatest(getMovies.type, getMoviesHandler)
    yield takeLatest(addMovie.type, setMovieHandler)

}