export const selectSearch = state => state.movies.searchedText

export const selectFilterMovies = state => {
    const filter = state.movies.searchedText;
    const movies = state.movies.movies;
    const sort = state.movies.filter;
    const pageNumber=state.movies.pageNum
    const pageSize=5

    
    switch (sort) {
        case "nameAsc":

            return [...movies].sort((a,b)=> {
                
                const nameA = a.title.substring(0,1);
                const nameB = b.title.substring(0,1); 
                if (nameA < nameB) {
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }
                // names must be equal
                return 0;
            }).slice((pageNumber-1)*pageSize,pageSize*pageNumber);
         
            
        case "nameDesc":
            return [...movies].sort((a,b)=> {
                
                const nameA = a.title.substring(0,1); // ignore upper and lowercase
                const nameB = b.title.substring(0,1); // ignore upper and lowercase
                if (nameA < nameB) {
                    return 1;
                }
                if (nameA > nameB) {
                    return -1;
                }
                // names must be equal
                return 0;
            }).slice((pageNumber-1)*pageSize,pageSize*pageNumber);
        case "durationAsc":
            return [...movies].sort((a,b)=> a.duration - b.duration).slice((pageNumber-1)*pageSize,pageSize*pageNumber)
        case "durationDesc":
            return [...movies].sort((a,b)=> b.duration - a.duration).slice((pageNumber-1)*pageSize,pageSize*pageNumber)
        default:
            return movies.filter(item=>item.title.toLowerCase().includes(filter.toLowerCase())).slice((pageNumber-1)*pageSize,pageSize*pageNumber)
    }
    
   
}


export const isMovieSelected = (state, id) => {
    return state.movies.selectedMovies.some((movie) => movie === id);
  };
  
export const selectedMoviesCount = (state) => state.movies.selectedMovies.length;


export const pageNumber= state => state.movies.pageNum