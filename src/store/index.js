import { configureStore, getDefaultMiddleware} from "@reduxjs/toolkit";
import moviesReducer from "./movies/slice";
import createSagaMiddleware from 'redux-saga';
import sagas from './rootSaga';
const sagaMiddleware = createSagaMiddleware();


export default configureStore({
  reducer: {
    movies: moviesReducer,
  },
  middleware : [
    ...getDefaultMiddleware(
     { thunk: false }
    ),
    sagaMiddleware
 ]

});




for (let saga in sagas) {
  sagaMiddleware.run(sagas[saga]);
}
