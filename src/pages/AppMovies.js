import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import AddMovie from '../components/AddMovie';
import MovieRow from '../components/MovieRow';
import MovieSearch from '../components/MovieSearch';
import Pagination from '../components/Pagination';
import { selectedMoviesCount, selectFilterMovies, selectSearch} from '../store/movies/selectors';
import { deselectAll, getMovies, selectAll, setAllMovies, setFilter } from '../store/movies/slice';



function AppMovies() {
    const dispatch = useDispatch()
    const movies = useSelector(selectFilterMovies)
    const searchedText=useSelector(selectSearch)
    const count = useSelector(selectedMoviesCount)
    // const fetchMovies = async () => {
    //     const data = await movieService.getAll()
        
    //     dispatch(setAllMovies(data))
    //   };


   useEffect(() => {
     dispatch(getMovies())
      // fetchMovies();
    },[])
    


  return (
    <div>
      <div className="movie-search">
        <MovieSearch/>
      </div>

      <div className="add-movie">
          <AddMovie/>
      </div>

      <div className="counter">
      <p>Selected movies: <strong>{count}</strong></p>
                <button className='btn btn-info mr-2' onClick={() => dispatch(deselectAll())}>Deselect all</button>
                <button className='btn btn-info' onClick={() => dispatch(selectAll())}>Select all</button>
      </div>

      <div className="sort-movies mt-2">
        <button className="btn btn-success mr-2" onClick={()=>dispatch(setFilter("nameAsc"))}>Name &uarr;</button>
        <button className="btn btn-success mr-2" onClick={()=>dispatch(setFilter("nameDesc"))} >Name &darr;</button>
        <button className="btn btn-success mr-2" onClick={()=>dispatch(setFilter("durationAsc"))} >Duration &uarr;</button>
        <button className="btn btn-success mr-2" onClick={()=>dispatch(setFilter("durationDesc"))} >Duration &darr;</button>
      </div>
       
         {movies.length>0  ?  movies.map(item=>(
              <MovieRow  key={item.id} movie={item}/>
           )) 
          : <p>There is no movie title that includes <strong>{searchedText}</strong></p>
          }
           
       <Pagination/>
    </div>
  )
}

export default AppMovies